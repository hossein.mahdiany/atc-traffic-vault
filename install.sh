#!/bin/bash

# This script is used to intall Traffic Vault NoQSL database
echo -e "\033[0;32m Starting Traffic Vault installation script \033[0m"
sleep 3

# Getting server IP address and url
read -p "Please enter the IP address of your server: " IP
read -p "Please enter the URL address of your server: " URL

# Downloading RPM package
echo -e "\033[0;32m Downloading Riak KV RPM package \033[0m"
wget https://store1.khallagh.com/packages/riak-2.2.6-1.el7.x86_64.rpm 
if [ $? = '0' ]
	then
		echo -e "\033[0;31m Download was successfull \033[0m"
else
		echo -e "\033[0;31m Can not download RPM package. \033[0m"
		exit 1
fi

# Installing
echo -e "\033[0;32m Installing Riak KV \033[0m"
yum localinstall -y riak-2.2.6-1.el7.x86_64.rpm
if [ $? = '0' ]
	then
		echo -e "\033[0;32m Installation was successfull \033[0m"
else
		echo -e "\033[0;31m Can not install Riak KV. \033[0m"
		exit 1
fi

# Configuring /etc/security/limits.conf
echo -e "\033[0;32m Configuring limit.conf \033[0m"

echo "riak soft nofile 65536" >> /etc/security/limits.conf
echo "riak hard nofile 200000" >> /etc/security/limits.conf

# Riak basic configurarion
echo -e "\033[0;32m Configuring Riak KV. Configuration file is /etc/riak.riak.kv \033[0m"

sed -i "s/^nodename = riak@127.0.0.1/nodename = riak@$IP/g" /etc/riak/riak.conf
sed -i "s/^## erlang.schedulers.force_wakeup_interval = 500/erlang.schedulers.force_wakeup_interval = 500/g" /etc/riak/riak.conf
sed -i "s/^## erlang.schedulers.compaction_of_load = false/erlang.schedulers.compaction_of_load = false/g" /etc/riak/riak.conf
sed -i "s/^## ring_size = 64/ring_size = 64/g" /etc/riak/riak.conf
sed -i "s/^listener.http.internal = 127.0.0.1:8098/listener.http.internal = $IP:8098/g" /etc/riak/riak.conf
sed -i "s/^listener.protobuf.internal = 127.0.0.1:8087/listener.protobuf.internal = $IP:8087/g" /etc/riak/riak.conf
sed -i "s/^## listener.https.internal = 127.0.0.1:8098/listener.https.internal = $IP:8088/g" /etc/riak/riak.conf

# Riak SSL configuration
echo -e "\033[0;32m Configuring Riak KV. Configur SSL certificates \033[0m"

mkdir /etc/riak/certs
echo "ssl.certfile = /etc/riak/certs/server.crt" >> /etc/riak/riak.conf
echo "ssl.keyfile = /etc/riak/certs/server.key" >> /etc/riak/riak.conf
echo "ssl.cacertfile = /etc/pki/tls/certs/chain.crt" >> /etc/riak/riak.conf
echo "tls_protocols.tlsv1.1 = on" >> /etc/riak/riak.conf

# Getting SSL certificate files 
echo -e "\033[0;32m Enter SSL certificate file and press enter and ctrl+d \033[0m"
certificate=$(cat)
echo $certificate >> /etc/riak/certs/server.crt
if [ $? = '0' ]
	then
		echo -e "\033[0;32m Certificate added successfully \033[0m"
else
		echo -e "\033[0;31m Can not add certificate file \033[0m"
		exit 1
fi

echo -e "\033[0;32m Enter SSL private key file and press enter and ctrl+d \033[0m"
privatekey=$(cat)
echo $privatekey >> /etc/riak/certs/server.key
if [ $? = '0' ]
	then
		echo -e "\033[0;32m Private key added successfully \033[0m"
else
		echo -e "\033[0;31m Can not add Private key file \033[0m"
		exit 1
fi

echo -e "\033[0;32m Enter SSL certificate chain file and press enter and ctrl+d \033[0m"
chain=$(cat)
echo $chain >> /etc/pki/tls/certs/chain.crt
if [ $? = '0' ]
	then
		echo -e "\033[0;32m Certificate chain added successfully \033[0m"
else
		echo -e "\033[0;31m Can not add certificate chain file \033[0m"
		exit 1
fi

# Staring Riak 
echo -e "\033[0;36m So far so good. We are going to start Riak NoSQL database \033[0m"

riak chkconfig
riak start
riak ping

# Create user account
echo -e "\033[0;32m Creating Riak KV user accounts \033[0m"

riak-admin security enable
riak-admin security add-group admins
riak-admin security add-group keysusers
read -p "Please enter password for admin user: " ADMINPASS 
riak-admin security add-user admin password=$ADMINPASS groups=admins
read -p "Please enter password for riakuser user: " RIAKPASS
riak-admin security add-user riakuser password=$RIAKPASS groups=keysusers
riak-admin security add-source riakuser 0.0.0.0/0 password
riak-admin security add-source admin 0.0.0.0/0 password
riak-admin security grant riak_kv.list_buckets,riak_kv.list_keys,riak_kv.get,riak_kv.put,riak_kv.delete on any to admins
riak-admin security grant riak_kv.get,riak_kv.put,riak_kv.delete on default ssl to keysusers
riak-admin security grant riak_kv.get,riak_kv.put,riak_kv.delete on default dnssec to keysusers
riak-admin security grant riak_kv.get,riak_kv.put,riak_kv.delete on default url_sig_keys to keysusers
riak-admin security grant riak_kv.get,riak_kv.put,riak_kv.delete on default cdn_uri_sig_keys to keysusers

# Enabling search
echo -e "\033[0;32m Enabling search \033[0m"

yum install -y java-1.8.0-openjdk java-1.8.0-openjdk-devel
sed -i "s/^search = off/search = on/g" /etc/riak/riak.conf
echo -e "\033[0;32m Riak KV is restarting now \033[0m"
riak restart
echo -e "\033[0;32m Granting required access for searching \033[0m"
riak-admin security grant search.admin on schema to admin
riak-admin security grant search.admin on index to admin
riak-admin security grant search.query on index to admin
riak-admin security grant search.query on index sslkeys to admin
riak-admin security grant search.query on index to riakuser
riak-admin security grant search.query on index sslkeys to riakuser
riak-admin security grant riak_core.set_bucket on any to admin

if [ $? = '0' ]
	then
		echo -e "\033[0;32m Congratulations! Riak KV has been installed and configured successfully \033[0m"
		echo -e "\033[0;32m Now it is time to open firewall ports and import schema. \033[0m"
		echo -e "\033[0;32m You should open these three TCP ports: 8088, 8087, 8098 \033[0m"
else
	echo -e "\033[0;31m Search schema test was not successful \033[0m"
	exit 1
fi
